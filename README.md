# Particulate matter in Brazil with CAMS products

**Objectif of the repository:**

Offers tools to download, process, visualize particulate matter (PM2.5 and PM10) concentrations in Brazil.

**Development environment:**

Programs were developed under:
*  Linux Ubuntu 18.04LTS -- x86_64-pc-linux-gnu (64-bit)
*  R version 3.6.3 (2020-02-29) -- "Holding the Windsock" (Copyright (C) 2020 The R Foundation for Statistical Computing)

**Prerequisits:**

1.  libraries:
*  Python
*  wgrib2 (wgrib2 was installed with the script provided by Bob Saska: https://gist.github.com/r35krag0th/ce83baab3b7da3157b74c7838a246d45)

2.  User accounts on the ECMWF website, and on the ECMWF FTP service (see https://confluence.ecmwf.int/display/CKB/FTP+access+to+CAMS+global+data)

3.  Please read the Copernicus data license and accept it at the bottom of the page

The author of these programs can in no way be held responsible for any user misuse of the data distributed by CAMS.

**Method:**

***Sources of PM concentration data***

*Past data*

Past estimations of PM2.5 and PM10 concentrations (in kg.m-3) up to 5 days before the current day come from the CAMS Near-Real-Time database (https://apps.ecmwf.int/datasets/data/cams-nrealtime/levtype=sfc/). For each day, estimations are provided every 3 hours from 03:00 UTC to 24:00 UTC on the fifth day after.
All data sources are listed in Appendix A.

*Prevision*

Previsions of PM2.5 and PM10 hourly concentrations (in kg.m-3) up to 5 days, at a 0.4 degree spatial resolution, are provided by the global atmospheric composition system of the Copernicus Atmosphere Monitoring Service (CAMS, https://confluence.ecmwf.int/display/CKB/FTP+access+to+CAMS+global+data). This service provides the previsions from 04:00 UTC on the current day, to 24:00 UTC on the fifth day after. Previsions are calculated two times (at 00:00 UTC and 12:00 UTC) every day and are available on the CAMS FTP server for three days.]

***PM data pre-processing***

*Past data*

For each day considered in the past, the mean of the 8 estimations from step 3 (3:00 UTC) to 24 (24:00 UTC) is computed to provided the daily mean concentrations of PM2.5 and PM10 in Brazil (UTC-3).


*Prevision*

By considering the Brasilia time (UTC-3) as reference, one day in Brazil is defined as the period between 03:00 UTC and 03:00 UTC the next day. As, for the current day, the prevision data are available from the fourth hour (see previous paragraph), the data for the first hour of the Brazilian day (i.e. 03:00 UTC) is extracted from the previsions realized the day before (more especially the 24+3=27th hour predicted the day before). Then, data for the first hour of the day (Brasilia time) is concatenated with the prevision data provided on the current UTC day in order to construct a hourly time series of four complete Brazilian days (in fact, the hourly previsions for the fifth UTC day do not permit construct an entire day according to the Brazilian time zone).
Data are then cropped according to the Brazil geographical extent, and multiplied by 109 to be converted in µg.m-3.
Eventually, the daily average is computed for each pixel, resulting in the daily prevision of the average PM2.5 and PM10 concentration up to 4 complete days according to the Brasilia time zone.


