#!/usr/bin/env python
from ecmwfapi import ECMWFDataServer
server = ECMWFDataServer()
server.retrieve({
    "class": "mc",
    "dataset": "cams_nrealtime",
    "date": "YYYY-mm-dd",
    "expver": "0001",
    "levtype": "sfc",
    "param": "73.210/74.210",
    "step": "hour",
    "stream": "oper",
    "time": "00",
    "type": "fc",
    "format": "netcdf",
    "grid": "0.4/0.4",
    "target": "path_and_filename",
})
